const randomIntFromInterval = (min: number, max: number) =>
  Math.floor(Math.random() * (max - min + 1) + min);

const STORE_NAMES = ["Shopping", "Trading", "Super store"];

const buildProducts = () => {
  const iterator = new Array(50).fill(0);

  return iterator.map((_, index) => {
    return {
      id: index,
      name: `Product ${index}`,
      storeName: STORE_NAMES[randomIntFromInterval(0, STORE_NAMES.length - 1)],
      storeUrl: "https://www.google.com/",
      price: randomIntFromInterval(1, 10),
      thumbnail: `img/product${randomIntFromInterval(1, 4)}.jpg`,
    };
  });
};

const products = buildProducts();

export default products;
