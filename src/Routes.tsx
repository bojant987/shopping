import React, { Suspense, lazy } from "react";
import { HashRouter, Route, Routes, useLocation } from "react-router-dom";

import AppContainer from "./Components/AppContainer/AppContainer";
import Home from "./Components/Home/Home";
import Loader from "./Components/Shared/Loader/Loader";

const Store = lazy(
  () =>
    import(
      /* webpackChunkName: "store" */ /* webpackPrefetch: true */ "./Components/Store/Store"
    )
);
const History = lazy(
  () =>
    import(
      /* webpackChunkName: "history" */ /* webpackPrefetch: true */ "./Components/History/History"
    )
);

const SuspenseLoader = () => <Loader testId="suspense" />;

const RouteDefinition = () => {
  const location = useLocation();

  return (
    <Routes key={location.key} location={location}>
      <Route element={<AppContainer Component={Home} withHeader />} path="/" />

      <Route
        element={
          <Suspense
            fallback={<AppContainer Component={SuspenseLoader} withHeader />}
          >
            <AppContainer Component={Store} withHeader />
          </Suspense>
        }
        path="/store"
      />

      <Route
        element={
          <Suspense
            fallback={<AppContainer Component={SuspenseLoader} withHeader />}
          >
            <AppContainer Component={History} withHeader />
          </Suspense>
        }
        path="/history"
      />

      <Route element={<AppContainer Component={Home} withHeader />} path="*" />
    </Routes>
  );
};

const AppRoutes = () => (
  <HashRouter>
    <RouteDefinition />
  </HashRouter>
);

export default AppRoutes;
