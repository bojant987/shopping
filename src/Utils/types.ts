export type Product = {
  id: string | number;
  name: string;
  storeName: string;
  storeUrl: string;
  thumbnail: string;
  price: number;
};

export interface HistoryProduct extends Product {
  purchaseTime: string;
}
