const formatPrice = (price: number) => `$${price.toFixed(2)}`;

export default formatPrice;
