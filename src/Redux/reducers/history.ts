import { AnyAction } from "redux";

import actionTypes from "../actionTypes";
import { HistoryProduct } from "../../Utils/types";

const initialState: HistoryProduct[] = [];

export default function history(
  state: HistoryProduct[] = initialState,
  action: AnyAction
) {
  if (action.type === actionTypes.PURCHASE_COMPLETED) {
    return [...state, ...action.payload];
  }

  return state;
}
