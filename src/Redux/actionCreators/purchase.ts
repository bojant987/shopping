import actionTypes from "../actionTypes";
import { HistoryProduct } from "../../Utils/types";

export const purchaseCompleted = (products: HistoryProduct[]) => ({
  type: actionTypes.PURCHASE_COMPLETED,
  payload: products,
});
