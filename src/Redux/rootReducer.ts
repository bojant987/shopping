import { combineReducers } from "redux";

import history from "./reducers/history";
import products from "./reducers/products";

export default combineReducers({
  products,
  history,
});
