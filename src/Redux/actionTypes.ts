import keyMirror from "keymirror";

export default keyMirror({
  PURCHASE_COMPLETED: null,
});
