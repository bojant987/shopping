import React, { useState, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";

import ProductTile from "./ProductTile/ProductTile";
import Button from "../Shared/Button/Button";
import { RootState } from "../../Redux/store";
import { Product } from "../../Utils/types";
import { purchaseCompleted } from "../../Redux/actionCreators/purchase";
import { isAlreadyAddedToCart, isProductOwned, attachPurchaseTimestamp } from "./utils";

import "./Store.scss";


const Store = () => {
  const dispatch = useDispatch();
  const products = useSelector((state: RootState) => state.products);
  const history = useSelector((state: RootState) => state.history);
  const [cart, setCart] = useState<Product[]>([]);

  const addToCart = useCallback((product: Product) => {
    setCart((state) => [...state, product]);
  }, []);

  const removeFromCart = useCallback((product: Product) => {
    setCart((state) => state.filter((pr) => pr.id !== product.id));
  }, []);

  const purchase = useCallback(() => {
    dispatch(purchaseCompleted(attachPurchaseTimestamp(cart)));
    setCart([]);
  }, [dispatch, cart]);

  return (
    <div className="Store" data-test-id="store">
      <h1 className="Store__heading">Browse our catalogue</h1>

      <div className="Store__header">
        <Button
          testId="purchase"
          onClick={purchase}
          size="l"
          isDisabled={!cart.length}
        >
          Buy {cart.length ? `(${cart.length})` : ""}
        </Button>
      </div>

      <div className="Store__container">
        {products.map((product) => (
          <ProductTile
            key={product.id}
            product={product}
            tileClassName="Store__item"
            addToCart={addToCart}
            removeFromCart={removeFromCart}
            isAddedToCart={isAlreadyAddedToCart(product, cart)}
            isOwned={isProductOwned(product, history)}
          />
        ))}
      </div>
    </div>
  );
};

export default Store;
