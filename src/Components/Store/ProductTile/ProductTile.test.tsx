import React from "react";
import { render, screen } from "@testing-library/react";

import ProductTile from "./ProductTile";

describe("ProductTile", () => {
  const compProps = {
    product: {
      id: 1,
      name: "test",
      storeName: "test store",
      storeUrl: "www.google.com",
      thumbnail: "/img/product1.jpg",
      price: 5,
    },
    addToCart: () => {},
    removeFromCart: () => {},
    isAddedToCart: false,
    isOwned: false,
  };

  it("renders with minimal required props", () => {
    render(<ProductTile {...compProps} />);
    expect(screen.getByTestId("product_tile")).toBeInTheDocument();
  });
});
