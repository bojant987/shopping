import React, { useCallback } from "react";
import classnames from "classnames";

import Button from "../../Shared/Button/Button";
import { Product } from "../../../Utils/types";
import formatPrice from "../../../Utils/formatPrice";

import "./ProductTile.scss";

type Props = {
  product: Product;
  addToCart: (product: Product) => void;
  removeFromCart: (product: Product) => void;
  isAddedToCart: boolean;
  isOwned: boolean;
  tileClassName?: string;
};

const ProductTile = ({
  product,
  tileClassName,
  addToCart,
  removeFromCart,
  isAddedToCart,
  isOwned,
}: Props) => {
  const classes = classnames("ProductTile", {
    [tileClassName as string]: !!tileClassName,
  });

  const cartHandler = useCallback(() => {
    if (!isAddedToCart) {
      addToCart(product);
    } else {
      removeFromCart(product);
    }
  }, [addToCart, removeFromCart, product, isAddedToCart]);

  const getButtonContent = useCallback(
    (price) => {
      if (isOwned) {
        return "Out of stock";
      }

      if (isAddedToCart) {
        return "Remove";
      }

      return (
        <>
          {formatPrice(price)}
          <span className="ProductTile__icon" />
        </>
      );
    },
    [isAddedToCart, isOwned]
  );

  return (
    <div className={classes} data-test-id="product_tile">
      <h3 className="ProductTile__name">{product.name}</h3>
      <img
        src={product.thumbnail}
        alt="product"
        className="ProductTile__thumbnail"
      />
      <Button
        testId="add_to_cart"
        onClick={cartHandler}
        size="l"
        className="ProductTile__addToCart"
        isDisabled={isOwned}
        kind={isAddedToCart ? "hollow" : undefined}
      >
        {getButtonContent(product.price)}
      </Button>
    </div>
  );
};

export default ProductTile;
