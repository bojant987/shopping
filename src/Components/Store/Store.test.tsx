import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

import Store from "./Store";

const mockStore = configureMockStore([]);

const store = mockStore({
  products: [],
  history: [],
});

describe("Store", () => {
  const compProps = {};

  it("renders with minimal required props", () => {
    render(
      <Provider store={store}>
        <Store {...compProps} />
      </Provider>
    );
    expect(screen.getByTestId("store")).toBeInTheDocument();
  });
});
