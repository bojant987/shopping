import { Product } from "../../Utils/types";

export const isAlreadyAddedToCart = (product: Product, cart: Product[]) =>
    cart.some((pr) => pr.id === product.id);

export const isProductOwned = (product: Product, history: Product[]) =>
    history.some((pr) => pr.id === product.id);

export const attachPurchaseTimestamp = (cart: Product[]) =>
    cart.map((pr) => ({ ...pr, purchaseTime: new Date().toISOString() }));