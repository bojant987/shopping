import React from "react";
import classNames from "classnames";

import Loader from "../Loader/Loader";

import "./Button.scss";

type Props = {
  testId: string;
  onClick?: () => void;
  children?: any;
  isLoading?: boolean;
  isDisabled?: boolean;
  className?: string;
  size?: "xs" | "m" | "l" | "xl";
  kind?: "alternative" | "hollow";
  type?: "button" | "submit" | "reset" | undefined;
};

const Button = ({
  children,
  isLoading,
  isDisabled,
  testId,
  className,
  onClick,
  size,
  kind,
  type = "button",
  ...rest
}: Props) => {
  const classes = classNames("Button", {
    "Button--disabled": isDisabled || isLoading,
    [`Button--${size}`]: size,
    [`Button--${kind}`]: kind,
    [className as string]: !!className,
  });

  return (
    <button
      onClick={onClick}
      disabled={isLoading || isDisabled}
      className={classes}
      data-test-id={`${testId}_button`}
      type={type}
      {...rest}
    >
      {isLoading ? (
        <Loader align="centerOnParent" size="extraSmall" testId={testId} />
      ) : (
        children
      )}
    </button>
  );
};

export default Button;
