import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Button from "./Button";

describe("Button", () => {
  const compProps = {
    testId: "test",
    className: "testClass",
    isLoading: false,
    isDisabled: false,
    onClick: () => {},
  };

  it("renders with minimal required props", () => {
    render(<Button {...compProps} />);

    expect(screen.getByTestId("test_button")).toBeInTheDocument();
  });

  it("show loader inside button if isLoading=true", () => {
    render(<Button {...compProps} isLoading />);

    expect(screen.getByTestId("test_loader")).toBeInTheDocument();
  });

  it("renders disabled button if isDisabled=true", () => {
    render(<Button {...compProps} isDisabled />);

    expect(screen.queryByTestId("test_button")).toHaveAttribute("disabled", "");
  });

  it("fire onClick fnc when clicked", () => {
    const spy = jest.fn();
    render(<Button {...compProps} onClick={spy} />);

    fireEvent.click(screen.getByTestId("test_button"));

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
