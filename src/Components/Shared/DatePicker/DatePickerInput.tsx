import React, { ForwardedRef, forwardRef } from "react";
import DayPickerInput from "react-day-picker/DayPickerInput";
import { format as dateFnsFormat, Locale } from "date-fns";

import Input from "../Input/Input";

import "react-day-picker/lib/style.css";
import "./DatePicker.scss";

const modifiersStyles = {
  selected: {
    backgroundColor: "#3C8EE2",
  },
  today: {
    color: "#e42045",
  },
  disabled: {
    color: "#555555",
  },
};

export const FORMAT = "LLL do yyyy";

function formatDate(date: Date, format: string, locale?: Locale) {
  return dateFnsFormat(date, format, { locale });
}

const CustomInput = forwardRef((props, ref: ForwardedRef<HTMLInputElement>) => (
  <Input
    wrapperClassName="DatePickerInput h-marginAll--xs"
    testId="date-picker"
    name="date-picker-input"
    {...props}
    ref={ref}
  />
));

const DatePickerInput = (props: any) => (
  <DayPickerInput
    {...props}
    dayPickerProps={{ modifiersStyles, selectedDays: props.selectedDays }}
    component={CustomInput}
    formatDate={formatDate}
    format={FORMAT}
    placeholder="M DD YYYY"
    inputProps={{ wrapperClassName: props.wrapperClassName }}
  />
);

export default DatePickerInput;
