import React from "react";
import DayPicker from "react-day-picker";

import "react-day-picker/lib/style.css";
import "./DatePicker.scss";

const modifiersStyles = {};

const DatePicker = (props: any) => (
  <DayPicker {...props} modifiersStyles={modifiersStyles} />
);

export default DatePicker;
