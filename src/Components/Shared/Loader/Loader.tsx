import React from "react";

import "./Loader.scss";

type Props = {
  testId: string;
  align?: "centerOnPage" | "centerOnParent";
  size?: "large" | "medium" | "small" | "extraSmall";
};

const Loader = ({
  align = "centerOnParent",
  size = "large",
  testId,
}: Props) => (
  <span
    className={`Loader Loader--${align} Loader--${size}`}
    data-test-id={`${testId}_loader`}
  />
);

export default Loader;
