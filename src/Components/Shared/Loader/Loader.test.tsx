import React from "react";
import { render } from "@testing-library/react";

import Loader from "./Loader";

describe("Loader", () => {
  it("renders with minimal required props", () => {
    const { queryByTestId, getByTestId } = render(<Loader testId="test" />);

    expect(queryByTestId("test_loader")).toBeInTheDocument();
    expect(getByTestId("test_loader")).toHaveClass("Loader--large");
    expect(getByTestId("test_loader")).toHaveClass("Loader--centerOnParent");
  });
});
