import React, { forwardRef } from "react";
import classnames from "classnames";

import "./Input.scss";

type Props = {
  testId: string;
  name: string;
  inputClassName?: string;
  wrapperClassName?: string;
  labelClassName?: string;
  isDisabled?: boolean;
  label?: string;
  type?: string;
  [key: string]: any;
};

const Input = forwardRef(
  (
    {
      inputClassName,
      wrapperClassName,
      labelClassName,
      isDisabled,
      testId,
      label,
      type = "text",
      name,
      ...rest
    }: Props,
    ref: React.ForwardedRef<HTMLInputElement>
  ) => {
    const inputClasses = classnames("Input", inputClassName, {
      "Input--disabled": isDisabled,
    });

    const wrapperClasses = classnames("Input__wrapper", wrapperClassName);
    const labelClasses = classnames("Input__label", labelClassName);

    return (
      <div className={wrapperClasses}>
        {label && (
          <label
            className={labelClasses}
            htmlFor={testId}
            data-test-id={`${testId}_label`}
          >
            {label}
          </label>
        )}

        <input
          ref={ref}
          id={testId}
          className={inputClasses}
          disabled={isDisabled}
          data-test-id={`${testId}_input`}
          type={type}
          name={name}
          autoComplete={`nope_${testId}`}
          {...rest}
        />
      </div>
    );
  }
);

export default Input;
