import React from "react";
import { render, screen } from "@testing-library/react";

import Input from "./Input";

describe("Input", () => {
  const compProps = {
    testId: "asdf",
    name: "username",
  };

  it("renders with minimal required props", () => {
    render(<Input {...compProps} />);

    expect(screen.getByTestId(`${compProps.testId}_input`)).toBeInTheDocument();
    expect(
      screen.queryByTestId(`${compProps.testId}_icon`)
    ).not.toBeInTheDocument();
    expect(
      screen.queryByTestId(`${compProps.testId}_label`)
    ).not.toBeInTheDocument();
    expect(
      screen.queryByTestId(`${compProps.testId}_error`)
    ).not.toBeInTheDocument();
  });

  it("renders label if passed", () => {
    render(<Input {...compProps} label="Enter username" />);

    expect(screen.getByTestId(`${compProps.testId}_label`)).toBeInTheDocument();
  });
});
