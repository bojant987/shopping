import React from "react";
import { render, screen } from "@testing-library/react";

import Table from "./Table";

describe("Table", () => {
  const compProps = {
    data: [],
    columns: [],
    testId: "test",
  };

  it("renders with minimal required props", () => {
    render(<Table {...compProps} />);
    expect(screen.getByTestId("table_test")).toBeInTheDocument();
  });
});
