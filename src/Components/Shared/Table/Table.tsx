import React, { useMemo, useState } from "react";
import classNames from "classnames";

import { Row, Column } from "./types";
import TableRow from "./TableRow";
import Pagination from "./Pagination/Pagination";

import "./Table.scss";

type Props = {
  data: Row[];
  columns: Column[];
  testId: string;
  pageSize?: number;
  wrapperClassName?: string;
};

const getPageData = (data: Row[], page: number, pageSize: number) =>
  data.slice(page * pageSize, page * pageSize + pageSize);

const Table = ({
  data,
  columns,
  testId,
  wrapperClassName,
  pageSize = 10,
}: Props) => {
  const [page, setPage] = useState(0);
  const pageData = useMemo(
    () => getPageData(data, page, pageSize),
    [data, page, pageSize]
  );
  const total = useMemo(() => data.length, [data]);
  const pageCount = useMemo(
    () => Math.ceil(total / pageSize) || 1,
    [total, pageSize]
  );
  const canPreviousPage = useMemo(() => page > 0, [page]);
  const canNextPage = useMemo(() => page < pageCount - 1, [page, pageCount]);

  const wrapperClasses = classNames("Table__wrapper", {
    [wrapperClassName as string]: !!wrapperClassName,
  });

  return (
    <div className={wrapperClasses}>
      <Pagination
        pageCount={pageCount}
        canPreviousPage={canPreviousPage}
        canNextPage={canNextPage}
        pageIndex={page}
        gotoPage={setPage}
        testId={testId}
      />

      <table className="Table" data-test-id={`table_${testId}`}>
        <thead className="Table__thead">
          <tr className="Table__tr">
            {columns.map((col) => (
              <td className="Table__td" align="center" key={col.id}>
                {col.label}
              </td>
            ))}
          </tr>
        </thead>

        <tbody className="Table__tbody">
          {pageData.map((row) => (
            <TableRow row={row} columns={columns} key={row.id} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
