import React from "react";

import { Row, Column } from "./types";

type Props = {
  row: Row;
  columns: Column[];
};

const TableRow = ({ row, columns }: Props) => (
  <tr className="Table__tr">
    {columns.map((col) => {
      if (col.accessor) {
        return (
          <td className="Table__td" align="center" key={col.id}>
            {row[col.accessor]}
          </td>
        );
      }

      if (col.render) {
        return (
          <td className="Table__td" align="center" key={col.id}>
            {col.render(row)}
          </td>
        );
      }

      return null;
    })}
  </tr>
);

export default TableRow;
