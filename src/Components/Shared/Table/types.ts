export type Row = {
  id: string | number;
  [key: string]: any;
};

export type Column = {
  id: string;
  label: string;
  accessor?: string;
  render?: (row: Row) => any;
};
