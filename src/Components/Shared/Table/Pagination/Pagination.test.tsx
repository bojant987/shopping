import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";

import Pagination from "./Pagination";

describe("Pagination", () => {
  const compProps = {
    canPreviousPage: true,
    canNextPage: true,
    gotoPage: () => {},
    nextPage: () => {},
    previousPage: () => {},
    pageIndex: 0,
    pageCount: 3,
    onPageChange: () => {},
    testId: "test",
  };

  it("renders pagination with proper number of buttons", () => {
    render(<Pagination {...compProps} />);

    expect(
      screen.getByTestId(`pagination_${compProps.testId}`)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(`pagination_${compProps.testId}_previous_button`)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(`pagination_${compProps.testId}_next_button`)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(`pagination_${compProps.testId}_first_button`)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(`pagination_${compProps.testId}_last_button`)
    ).toBeInTheDocument();
  });

  it("calls gotoPage when page is clicked", () => {
    const spy = jest.fn();
    render(<Pagination {...compProps} gotoPage={spy} pageCount={10} />);

    fireEvent.click(
      screen.getByTestId(`pagination_${compProps.testId}_next_button`)
    );

    expect(spy).toHaveBeenCalledWith(1);

    fireEvent.click(
      screen.getByTestId(`pagination_${compProps.testId}_last_button`)
    );

    expect(spy).toHaveBeenCalledWith(9);
  });

  it("resets to first page if pageCount changes", () => {
    const spy = jest.fn();
    const { rerender } = render(<Pagination {...compProps} gotoPage={spy} />);

    expect(spy).toHaveBeenCalledWith(0);

    rerender(<Pagination {...compProps} gotoPage={spy} pageCount={2} />);

    expect(spy).toHaveBeenCalledWith(0);
  });

  it("doesn't display pages if there's only one page", () => {
    render(<Pagination {...compProps} pageCount={1} />);

    expect(
      screen.queryByTestId(`pagination_${compProps.testId}`)
    ).not.toBeInTheDocument();
  });
});
