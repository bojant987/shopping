import React, { useEffect, memo } from "react";

import "./Pagination.scss";

type Props = {
  canPreviousPage: boolean;
  canNextPage: boolean;
  gotoPage: (page: number) => void;
  pageIndex: number;
  pageCount: number;
  testId: string;
};

const Pagination = ({
  canPreviousPage,
  canNextPage,
  gotoPage,
  pageIndex,
  pageCount,
  testId,
}: Props) => {
  /**
   * @description reset to first page if number of existing pages have changed
   */
  useEffect(() => {
    gotoPage(0);
  }, [gotoPage, pageCount]);

  return pageCount > 1 ? (
    <div className="PaginationContainer" data-test-id={`pagination_${testId}`}>
      <button
        aria-label="First"
        onClick={() => gotoPage(0)}
        key="page-first"
        className="PaginationButton PaginationButton--first"
        disabled={pageIndex === 0}
        data-test-id={`pagination_${testId}_first_button`}
      />

      <button
        aria-label="Previous"
        onClick={() => gotoPage(pageIndex - 1)}
        key="page-previous"
        className="PaginationButton PaginationButton--previous"
        disabled={!canPreviousPage}
        data-test-id={`pagination_${testId}_previous_button`}
      />

      <p
        data-test-id={`pagination_${testId}_pagesInfo`}
        className="Pagination__pagesInfo h-textCenter"
      >{`Page ${pageIndex + 1}/${pageCount}`}</p>

      <button
        onClick={() => gotoPage(pageIndex + 1)}
        key="page-next"
        aria-label="Next"
        className="PaginationButton PaginationButton--next"
        disabled={!canNextPage}
        data-test-id={`pagination_${testId}_next_button`}
      />

      <button
        onClick={() => gotoPage(pageCount - 1)}
        key="page-last"
        aria-label="Last"
        className="PaginationButton PaginationButton--last"
        disabled={pageIndex === pageCount - 1}
        data-test-id={`pagination_${testId}_last_button`}
      />
    </div>
  ) : null;
};

export default memo(Pagination);
