import React, { ComponentType } from "react";

import Header from "./Header/Header";

import "./AppContainer.scss";

type Props = {
  Component: ComponentType;
  withHeader?: boolean;
};

const AppContainer = ({ Component, withHeader }: Props) => (
  <div className="AppContainer">
    {withHeader && <Header />}
    <main className="AppContainer__main" data-test-id="main">
      <Component />
    </main>
  </div>
);

export default AppContainer;
