import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Header from "./Header";

describe("Header", () => {
  const compProps = {};

  it("renders with minimal required props", () => {
    render(
      <MemoryRouter>
        <Header {...compProps} />
      </MemoryRouter>
    );
    expect(screen.getByTestId("header")).toBeInTheDocument();
  });
});
