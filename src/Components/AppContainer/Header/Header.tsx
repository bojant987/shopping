import React from "react";

import CustomLink from "./CustomLink";
import logo from "../../../assets/img/logo.png";

import "./Header.scss";

const Header = () => (
  <div className="Header" data-test-id="header">
    <img src={logo} alt="logo" className="Header__logo" />

    <div className="Header__menu">
      <CustomLink to="/" className="Header__menuItem">
        Home
      </CustomLink>
      <CustomLink to="/store" className="Header__menuItem">
        Store
      </CustomLink>
      <CustomLink to="/history" className="Header__menuItem">
        History
      </CustomLink>
    </div>
  </div>
);

export default Header;
