import { LinkProps, Link, useResolvedPath, useMatch } from "react-router-dom";
import classnames from "classnames";

const CustomLink = ({ children, to, className, ...props }: LinkProps) => {
  const resolved = useResolvedPath(to);
  const match = useMatch({ path: resolved.pathname, end: true });
  const classes = classnames({
    [className as string]: !!className,
    active: match,
  });

  return (
    <Link
      style={{ textDecoration: match ? "underline" : "none" }}
      to={to}
      {...props}
      className={classes}
    >
      {children}
    </Link>
  );
};

export default CustomLink;
