import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Home from "./Home";

describe("Home", () => {
  const compProps = {};

  it("renders with minimal required props", () => {
    render(
      <MemoryRouter>
        <Home {...compProps} />
      </MemoryRouter>
    );
    expect(screen.getByTestId("home")).toBeInTheDocument();
  });
});
