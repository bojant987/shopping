import React, { useCallback } from "react";
import { useNavigate } from "react-router-dom";

import Carousel from "./Carousel/Carousel";
import Button from "../Shared/Button/Button";

import "./Home.scss";

const Home = () => {
  const navigate = useNavigate();

  const toStore = useCallback(() => {
    navigate("store");
  }, [navigate]);

  return (
    <div className="Home" data-test-id="home">
      <Carousel />

      <h1 className="Home__heading">
        Welcome to our corner. Explore our store!
      </h1>

      <Button testId="to_store" onClick={toStore} kind="hollow" size="xl">
        Browse
      </Button>
    </div>
  );
};

export default Home;
