import React from "react";
import { Carousel as ResponsiveCarousel } from "react-responsive-carousel";

import img1 from "../../../assets/img/img1.jpg";
import img2 from "../../../assets/img/img2.jpg";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./Carousel.scss";

const Carousel = () => (
  <div className="Carousel">
    <ResponsiveCarousel showThumbs={false} showArrows>
      <div>
        <img src={img1} alt="first" />
        <p className="legend">Legend 1</p>
      </div>
      <div>
        <img src={img2} alt="second" />
        <p className="legend">Legend 2</p>
      </div>
    </ResponsiveCarousel>
  </div>
);

export default Carousel;
