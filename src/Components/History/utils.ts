import { HistoryProduct } from "../../Utils/types";
import { isSameDay } from "date-fns";

export type Filters = {
    name: string;
    storeName: string;
    purchaseTime: Date | number;
};

export const filterHistory = (filters: Filters, history: HistoryProduct[]) => {
    let filtered = [...history];

    if (filters.name) {
        filtered = filtered.filter((hrProd) =>
            hrProd.name.toLowerCase().includes(filters.name.toLowerCase())
        );
    }

    if (filters.storeName) {
        filtered = filtered.filter((hrProd) =>
            hrProd.storeName.toLowerCase().includes(filters.storeName.toLowerCase())
        );
    }

    if (filters.purchaseTime) {
        filtered = filtered.filter((hrProd) =>
            isSameDay(new Date(hrProd.purchaseTime), filters.purchaseTime)
        );
    }

    return filtered;
};