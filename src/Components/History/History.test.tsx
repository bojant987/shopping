import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

import History from "./History";

const mockStore = configureMockStore([]);

const store = mockStore({
  products: [],
  history: [],
});

describe("History", () => {
  const compProps = {};

  it("renders with minimal required props", () => {
    render(
      <Provider store={store}>
        <History {...compProps} />
      </Provider>
    );
    expect(screen.getByTestId("history")).toBeInTheDocument();
  });
});
