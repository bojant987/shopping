import React, { FormEventHandler, useCallback, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../Redux/store";
import { format } from "date-fns";

import Input from "../Shared/Input/Input";
import DatePickerInput, { FORMAT } from "../Shared/DatePicker/DatePickerInput";
import Table from "../Shared/Table/Table";
import Button from "../Shared/Button/Button";
import { Row } from "../Shared/Table/types";
import { HistoryProduct } from "../../Utils/types";
import formatPrice from "../../Utils/formatPrice";
import { filterHistory, Filters } from "./utils";

import "./History.scss";

const COLUMNS = [
  {
    id: "name",
    accessor: "name",
    label: "Name",
  },
  {
    id: "store_name",
    label: "Store",
    render: (row: Row) => (
      <a href={row.storeUrl} target="_blank" rel="noreferrer">
        {row.storeName}
      </a>
    ),
  },
  {
    id: "purchase_time",
    label: "Purchase time",
    render: (row: Row) => {
      const date = new Date(row.purchaseTime);

      return format(date, FORMAT);
    },
  },
  {
    id: "price",
    label: "Price",
    render: (row: Row) => formatPrice(row.price),
  },
];

const History = () => {
  const history = useSelector((state: RootState) => state.history);
  const [filters, setFilters] = useState<Filters>({
    name: "",
    storeName: "",
    purchaseTime: 0,
  });
  const [data, setData] = useState<HistoryProduct[]>(history);

  const handleSubmit: FormEventHandler<HTMLFormElement> = useCallback(
    (event) => {
      event.preventDefault();
      setData(filterHistory(filters, history));
    },
    [filters, history]
  );

  const handleInputChange = useCallback((event) => {
    const { name, value } = event.target;

    setFilters((state) => ({ ...state, [name]: value }));
  }, []);

  const handleDayClick = useCallback((day, { disabled }) => {
    if (!disabled) {
      setFilters((state) => ({ ...state, purchaseTime: day || 0 }));
    }
  }, []);

  return (
    <div className="History" data-test-id="history">
      <h1 className="History__heading">Your purchase history</h1>

      {history.length ? (
        <form onSubmit={handleSubmit} className="History__form">
          <div className="History__inputWrapper">
            <Input
              testId="name"
              name="name"
              onChange={handleInputChange}
              placeholder="Product name"
              wrapperClassName="History__input"
            />
            <Input
              testId="storeName"
              name="storeName"
              onChange={handleInputChange}
              placeholder="Store name"
              wrapperClassName="History__input"
            />
            <DatePickerInput
              onDayChange={handleDayClick}
              selectedDays={filters.purchaseTime}
              wrapperClassName="History__input"
            />
          </div>

          <Button
            testId="filter_history"
            type="submit"
            className="History__filterBtn"
          >
            Filter
          </Button>
        </form>
      ) : null}

      {data.length ? (
        <Table
          data={data}
          columns={COLUMNS}
          testId="history"
          wrapperClassName="History__table"
        />
      ) : (
        <p>No results.</p>
      )}
    </div>
  );
};

export default History;
